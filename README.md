# README #
* Simple REST API service for handling shopping list data (Android client app - https://joannarozes@bitbucket.org/joannarozes/shoplist_app.git)
* Version 0.1.0
* Dependencies: MongoDB
* Compatible IDE: Eclipse + PyDev