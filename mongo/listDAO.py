'''
Created on 2 gru 2014

@author: sctester
'''
from pymongo import MongoClient
import json
import datetime

class ListDao(object):

    def save_request(self, reqid):
        request = {'id': reqid, 'created': datetime.datetime.utcnow()}
        self.requests.save(request)

    def get_itemlists(self, username):
        itemlists = list(self.collection.find({'username':username}))
        for item in itemlists:
            item.pop('username', None);
            item.pop('_id', None);
        return itemlists

    def get_itemlist(self, username, listname):
        return self.collection.find_one({'name': listname, 'username':username})

    def remove_itemlist(self, username, listname, reqid):
        done = self.requests.find_one({'id':reqid})
        if not done:
            self.save_request(reqid)
            self.collection.remove({'name': listname, 'username':username})

    def add_itemlist(self, username, listname, reqid):
        itemlist = {'name':listname,'username': username};
        exists = self.collection.find_one(itemlist)
        done = self.requests.find_one({'id':reqid})
        print exists
        print done
        if not done and not exists:
            self.save_request(reqid)
            self.collection.save(itemlist)

    def add_item(self, username, listname, itemjson, reqid):
        itemlist = {'name':listname,'username': username};
        listexists = self.collection.find_one(itemlist)
        done = self.requests.find_one({'id':reqid})
        if not done and listexists:
            self.save_request(reqid)
            self.save_request(reqid)
            item = json.loads(itemjson)
            if listexists.has_key('items'):
                if (listexists['items'].has_key(item['name'])):
                    listexists['items'][item['name']] = listexists['items'][item['name']] + item['toBuy'];
                else:
                    listexists['items'][item['name']] = item['toBuy'];
            else:
                listexists['items'] = {}
                listexists['items'][item['name']] = item['toBuy'];
            self.collection.save(listexists)

    def remove_item(self, username, listname, itemname, reqid):
        itemlist = {'name':listname,'username': username};
        listexists = self.collection.find_one(itemlist)
        done = self.requests.find_one({'id':reqid})
        if not done and listexists:
            self.save_request(reqid)
            self.collection.update({'name': listname, 'username':username}, {"$unset": {itemname:""}})

    def inc_item(self, username, listname, itemname, reqid):
        exists = self.collection.find_one({'name': listname, 'username': username, ('items.'+itemname):{"$exists":"true"}})
        done = self.requests.find_one({'id':reqid})
        if not done and exists:
            self.save_request(reqid)
            self.collection.update({'_id':exists['_id']}, {"$inc": {("items."+itemname): 1}})

    def dec_item(self, username, listname, itemname, reqid):
        exists = self.collection.find_one({'name': listname, 'username': username, ('items.'+itemname):{"$exists":"true"}})
        done = self.requests.find_one({'id':reqid})
        if not done and exists:
            self.save_request(reqid)
            self.collection.update({'_id':exists['_id']}, {"$inc": {("items."+itemname): -1}})

    def __init__(self):
        self.mongoclient = MongoClient('localhost', 27017)
        self.db = self.mongoclient.listdb
        self.collection = self.db.list
        self.requests = self.db.request
        