'''
Created on 2 gru 2014

@author: Joasia
'''
import json
from pymongo import MongoClient
import uuid

class UserDao(object):
    def add_user(self, userjson):
        user = json.loads(userjson)
        exists = self.collection.find_one({'username': user['username']})
        if exists:
            return "User already exists"
        else:
            self.collection.save(user)
            return "OK"

    def _check_password(self, username, password):
        exists = self.collection.find_one({'username': username, 'password': password})
        if exists:
            return True
        else:
            return False

    def login_user(self, userjson):
        user = json.loads(userjson)
        if self._check_password(user['username'], user['password']):
            tokenuuid = str(uuid.uuid4())
            token = {'username': user['username'], 'token': tokenuuid};
            exists = self.tokencollection.find_one({'username': user['username']})
            if exists:
                token['_id'] = exists['_id'];
            self.tokencollection.save(token);
            return tokenuuid

    def check_token(self, username, token):
        exists = self.tokencollection.find_one({'username': username, 'token':token})
        if exists:
            return True
        else:
            return False

    def logout_user(self, username, token):
        if self.check_token(username, token):
            self.tokencollection.remove({'username': username, 'token': token})
            return "OK"
        else:
            return "Unauthorized!"

    def __init__(self):
        self.mongoclient = MongoClient('localhost', 27017)
        self.db = self.mongoclient.listdb
        self.collection = self.db.users
        self.tokencollection = self.db.tokens
