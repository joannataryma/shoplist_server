import web
import json
from mongo.listDAO import ListDao
from mongo.userDAO import UserDao

user_dao = UserDao();
list_dao = ListDao();

urls = (
    '/createuser', 'create_user',
    '/authuser', 'auth_user',
    '/logoutuser', 'logout_user',
    '/checktoken', 'check_token',
    '/getlist/(.*)', 'get_list',
    '/getlists', 'get_lists',
    '/addlist', 'add_list',
    '/deletelist/(.*)', 'delete_list',
    '/additem/(.*)', 'add_item',
    '/removeitem/(.*)', 'remove_item',
    '/incitem/(.*)', 'inc_item',
    '/decitem/(.*)', 'dec_item'
)

class auth_user:
    def POST(self):
        userjson = web.data()
        return user_dao.login_user(userjson)

class create_user:
    def POST(self):
        userjson = web.data()
        return user_dao.add_user(userjson);

class logout_user:
    def GET(self):
        username = web.ctx.env['HTTP_USERNAME'];
        token = web.ctx.env['HTTP_TOKEN'];
        return user_dao.logout_user(username, token);

class check_token:
    def GET(self):
        username = web.ctx.env['HTTP_USERNAME'];
        token = web.ctx.env['HTTP_TOKEN'];
        if user_dao.check_token(username, token):
            return "OK"
        else:
            return "Unauthorized"

class delete_list:
    def DELETE(self, listname):
        username = web.ctx.env['HTTP_USERNAME'];
        token = web.ctx.env['HTTP_TOKEN'];
        reqid = web.ctx.env['HTTP_REQID'];
        if user_dao.check_token(username, token):
            return list_dao.remove_itemlist(username, listname, reqid)
        else:
            return "Unauthorized"

class add_list:
    def POST(self):
        username = web.ctx.env['HTTP_USERNAME'];
        token = web.ctx.env['HTTP_TOKEN'];
        reqid = web.ctx.env['HTTP_REQID'];
        listjson =  json.loads(web.data())
        listname = listjson['name'];
        if user_dao.check_token(username, token):
            return list_dao.add_itemlist(username, listname, reqid)
        else:
            print "Unauthorized"
            return "Unauthorized"

class get_list:
    def GET(self, listname):
        username = web.ctx.env['HTTP_USERNAME'];
        token = web.ctx.env['HTTP_TOKEN'];
        if user_dao.check_token(username, token):
            return str(list_dao.get_itemlist(username, listname)).encode('ascii')
        else:
            return "Unauthorized"

class get_lists:
    def GET(self):
        username = web.ctx.env['HTTP_USERNAME'];
        token = web.ctx.env['HTTP_TOKEN'];
        if user_dao.check_token(username, token):
            return str(list_dao.get_itemlists(username)).encode('ascii')
        else:
            return "Unauthorized"

class add_item:
    def POST(self, listname):
        itemjson = web.data()
        username = web.ctx.env['HTTP_USERNAME'];
        token = web.ctx.env['HTTP_TOKEN'];
        reqid = web.ctx.env['HTTP_REQID'];
        if user_dao.check_token(username, token):
            return str(list_dao.add_item(username, listname, itemjson, reqid))
        else:
            return "Unauthorized"


class remove_item:
    def PUT(self, listname):
        itemjson =  json.loads(web.data())
        itemname = itemjson['name'];
        username = web.ctx.env['HTTP_USERNAME'];
        token = web.ctx.env['HTTP_TOKEN'];
        reqid = web.ctx.env['HTTP_REQID'];
        if user_dao.check_token(username, token):
            return str(list_dao.remove_item(username, listname, itemname, reqid))
        else:
            return "Unauthorized"


class inc_item:
    def PUT(self, listname):
        itemjson =  json.loads(web.data())
        itemname = itemjson['name'];
        username = web.ctx.env['HTTP_USERNAME'];
        token = web.ctx.env['HTTP_TOKEN'];
        reqid = web.ctx.env['HTTP_REQID'];
        if user_dao.check_token(username, token):
            return str(list_dao.inc_item(username, listname, itemname, reqid))
        else:
            return "Unauthorized"

class dec_item:
    def PUT(self, listname):
        itemjson =  json.loads(web.data())
        itemname = itemjson['name'];
        username = web.ctx.env['HTTP_USERNAME'];
        token = web.ctx.env['HTTP_TOKEN'];
        reqid = web.ctx.env['HTTP_REQID'];
        if user_dao.check_token(username, token):
            return str(list_dao.dec_item(username, listname, itemname, reqid))
        else:
            return "Unauthorized"

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.internalerror = web.debugerror
    app.run()